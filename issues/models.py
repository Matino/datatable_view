from django.db import models

class Entry(models.Model):
    
    Issue = models.CharField(max_length = 200)
    Date_created = models.TextField()
    Status = models.TextField()
    Date_in_current_status = models.TextField()

    def __str__(self):
        return self.Issue